package pk.labs.LabA;

public class LabDescriptor {

     // region P1
    public static String displayImplClassName = "pk.labs.LabA.display.Wyswietlacz";
    public static String controlPanelImplClassName = "pk.labs.LabA.ControlPanel.PanelSterowania";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.IMain";
    public static String mainComponentImplClassName = "pk.labs.LabA.main.Main";
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "mainframe";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly-frame";
    // endregion
}
